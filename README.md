# retuutbot

a bot that retoots things on mastodon

## behaviour
Attach this bot to any account.

It will read all toots from your home timeline and retoot those which contain any of the configured hashtags.
This serves to create "subscribe-to-hashtag" feed from only selected accounts.

## installation

0. Install dependencies (`python3 python3-venv python3-pip`)
1. Create user account (e.g. `retuutbot`)
2. Copy contents of `src` to installation folder (e.g. `/opt/retuutbot`)
3. Create virtual environment, install dependencies
3. Copy systemd service file (`etc/retuutbot.service`) to destination (`/etc/systemd/system/retuutbot.service`)
4. Copy systemd timer file (`etc/retuutbot.timer`) to destination (`/etc/systemd/system/retuutbot.timer`)
5. Reload services, then enable and start systemd timer

Install script for debian/ubuntu:
```
# initial setup
apt install python3 python3-venv python3-pip
adduser retuutbot --disabled-login --no-create-home --gecos ""
mkdir /opt/retuutbot
python3 -m venv /opt/retuutbot

# download/update retuutbot
git clone https://codeberg.org/retuutbot/retuutbot.git
cp retuutbot/* /opt/retuutbot/ -R
chown retuutbot:retuutbot /opt/retuutbot -R
su -c "/opt/retuutbot/bin/pip install -r /opt/retuutbot/requirements.txt" retuutbot

# remember to adjust the example config in etc/retuutbot.json.example and copy it to etc/retuutbot.json!

# caution: this will overwrite custom changes in the service and timer files on each update!
cp /opt/retuutbot/etc/retuutbot.service /etc/systemd/system/retuutbot.service
cp /opt/retuutbot/etc/retuutbot.timer /etc/systemd/system/retuutbot.timer
systemctl daemon-reload
systemctl enable retuutbot.timer
systemctl start retuutbot.timer
```

## configuration
Edit `retuutbot.timer` file to change the timeline check interval.

Copy `etc/retuutbot.json.example` to `etc/retuutbot.json` and adjust the values.
(Please refer to comments in the cfg file itself.)

## uninstallation instructions
Remove service, timer, files and the user account.

```
systemctl disable retuutbot.timer
systemctl stop retuutbot.timer
rm /etc/systemd/system/retuutbot.timer
rm /etc/systemd/system/retuutbot.service
systemctl daemon-reload
rm /opt/retuutbot -R
deluser retuutbot
```
