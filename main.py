#!/usr/bin/python3

import logging
logging.basicConfig(level=getattr(logging, 'DEBUG'))

from src import cfg, api_interaction, parser


mastodon = api_interaction.getMastodonInstance(cfg.MASTODON_ACCESS_TOKEN, cfg.MASTODON_HOST)

for toot in api_interaction.fetch(mastodon):
    logging.info('There is a toot!')
    if parser.contains_hashtags(cfg.MASTODON_HASHTAGS, toot.content):
        logging.info('Found toot, retooting')
        api_interaction.retoot(mastodon, toot.id)
