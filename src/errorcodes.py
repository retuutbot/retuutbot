# define error codes
ERROR_NO_CFG_FILE = 42
ERROR_BAD_JSON_SYNTAX = 43
ERROR_BAD_JSON_STRUCTURE = 44
ERROR_MASTODON_API_ERROR = 45
ERROR_OTHER = 46
