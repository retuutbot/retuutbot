from mastodon import Mastodon, MastodonUnauthorizedError
import logging
import sys

from .errorcodes import *

'''
returns handle of mastodon api
'''
def getMastodonInstance(access_token, api_base_url):
	return Mastodon(
		access_token = access_token,
		api_base_url = api_base_url
	)

'''
fetch timeline and return toots that have not been reblogged yet and that are no reblogs
'''
def fetch(mastodon):
	timeline_home = mastodon.timeline_home()
	toots = []
	
	for toot in timeline_home:
		if toot.reblogged is not True and toot.reblog is not True:
			toots.append(toot)
	
	return toots

'''
reblog a toot
'''
def retoot(mastodon, id):
	mastodon.status_reblog(id=id)