import logging
import json
import pathlib
import sys
import re

from .errorcodes import *

MASTODON_HOST = ''
MASTODON_ACCESS_TOKEN = ''
MASTODON_HASHTAGS = []


'''
return path to cfgfile
'''
def get_cfg_path():
    return pathlib.Path(__file__).parent.absolute().parent.joinpath('etc', 'retuutbot.json').absolute()


'''
validates a json dict if it is suitable for configuration
'''
def validate_cfg_json(j):
    if 'host' not in j:
        logging.critical('Key "host" is not in cfg')
        sys.exit(ERROR_BAD_JSON_STRUCTURE)
    if not isinstance(j['host'], str):
        logging.critical('Key "host" is not str')
        sys.exit(ERROR_BAD_JSON_STRUCTURE)
    if not re.match(r'^.+:\/\/.*[^/]$', j['host']):
        logging.critical('host has not the structure "https://example.tld" (with protocol, no trailing slash)')
        sys.exit(ERROR_BAD_JSON_STRUCTURE)

    if 'access_token' not in j:
        logging.critical('Key "access_token" is not in cfg')
        sys.exit(ERROR_BAD_JSON_STRUCTURE)
    if not isinstance(j['access_token'], str):
        logging.critical('Key "access_token" is not str')
        sys.exit(ERROR_BAD_JSON_STRUCTURE)

    if 'hashtags' not in j:
        logging.critical('Key "hashtags" is not in cfg')
        sys.exit(ERROR_BAD_JSON_STRUCTURE)
    if not isinstance(j['hashtags'], list):
        logging.critical('Key "hashtags" is not list')
        sys.exit(ERROR_BAD_JSON_STRUCTURE)

    for hashtag in j['hashtags']:
        if not isinstance(hashtag, str):
            logging.critical('Key "hashtags" must be list of string')
            sys.exit(ERROR_BAD_JSON_STRUCTURE)
        if re.match(r'^#', hashtag):
            logging.critical('hashtags must not start with a #')
            sys.exit(ERROR_BAD_JSON_STRUCTURE)


'''
try to load the cfg, but do not check for file/json errors
(config errors are caught and will crash)
'''
def load_cfg_no_catch():
    global MASTODON_HOST, MASTODON_ACCESS_TOKEN, MASTODON_HASHTAGS

    with open(get_cfg_path(), 'r') as fp:
        cfg_json = json.load(fp)
        validate_cfg_json(cfg_json)

        MASTODON_HOST = cfg_json['host']
        MASTODON_ACCESS_TOKEN = cfg_json['access_token']
        MASTODON_HASHTAGS = cfg_json['hashtags']


'''
load the cfg and crash on any sort of erros
'''
def load_cfg():
    logging.info('loading cfg...')

    try:
        load_cfg_no_catch()
    except FileNotFoundError:
        logging.critical('cfg file not found at "{}"'.format(get_cfg_path()))
        sys.exit(ERROR_NO_CFG_FILE)
    except json.decoder.JSONDecodeError as e:
        logging.critical('cfg file has bad syntax: {}'.format(str(e)))
        sys.exit(ERROR_BAD_JSON_SYNTAX)
    except Exception as e:
        raise e


load_cfg()

