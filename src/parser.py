## moji
## 05.09.2020

## parser.py

## load modules
import json
import re
from bs4 import BeautifulSoup


re_hashtag = '#[a-zA-Zäüö][a-zA-Zäöü0-9_]*'

def contains_hashtags(hashtags, tootcontent):

	# strip html stuff
	soup = BeautifulSoup(tootcontent, features="html.parser")

	# get hashtags
	extracted = re.findall(re_hashtag, soup.get_text())

	hashtags = [item.lower() for item in hashtags]
	extracted = [item.lower() for item in extracted]

	for hashtag in hashtags:
		if "#"+hashtag in extracted:
			return 1

	return 0
